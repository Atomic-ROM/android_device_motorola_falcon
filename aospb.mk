$(call inherit-product, device/motorola/falcon/full_falcon.mk)

# Inherit some common AOSPB stuff.
$(call inherit-product, vendor/aospb/config/common_full_phone.mk)

PRODUCT_RELEASE_NAME := MOTO G
PRODUCT_NAME := aospb_falcon

PRODUCT_GMS_CLIENTID_BASE := android-motorola
